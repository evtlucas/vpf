from django.db import models

class Account(models.Model):
    title = models.CharField(max_length=100)

    class Meta:
        verbose_name = 'MODELNAME'
        verbose_name_plural = 'MODELNAMEs'

    def __str__(self):
        return self.title


class Item(models.Model):
    ITEM_TYPES = [
        ('IN', 'Receita'),
        ('OU', 'Despesa'),
        ('TR', 'Transferência'),
    ]
    title = models.CharField(max_length=100)
    value = models.FloatField()
    date = models.DateField(auto_now_add=True)
    item_type = models.CharField(max_length=15,choices=ITEM_TYPES)
    account = models.ForeignKey(Account, on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Item'
        verbose_name_plural = 'Itens'

    def __str__(self):
        return self.title

